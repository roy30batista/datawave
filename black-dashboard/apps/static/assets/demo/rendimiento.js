var xmlhttp = new XMLHttpRequest();

var serverIP = location.host;

var url = "http://" + serverIP + "/static/assets/demo/historico.json";
xmlhttp.open("GET", url, true);
xmlhttp.send();

var maxPointsToShow = 10;
var valoresCpu = [];
var valoresMemoria = [];
var valoresDisco = [];

xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        var data = JSON.parse(this.responseText);
        var consumoMensual = data.consumo_mensual;

        valoresCpu = [];
        valoresMemoria = [];
        valoresDisco = [];

        var startIndex = Math.max(0, consumoMensual.length - maxPointsToShow);
        for (var i = startIndex; i < consumoMensual.length; i++) {

            /*valor para CPU */
            var dato = consumoMensual[i];
            valoresCpu.push(dato.cpu);

            /*valor para Memoria */
            var memoria = parseFloat(dato.memoria.replace(" gb", ""));
            valoresMemoria.push(memoria);

            /*valor para Disco */
            var disco = parseFloat(dato.disco.replace(" gb", ""));
            valoresDisco.push(disco);
        }

        const ctxCpu = document.getElementById('rendimiento_cpu');
        new Chart(ctxCpu, {
            type: 'line',
            data: {
                labels: Array(valoresCpu.length).fill(""),
                datasets: [
                    {
                        label: 'CPU',
                        data: valoresCpu,
                        backgroundColor: 'transparent',
                        borderColor: 'red',
                        borderWidth: 4
                    }
                ]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

        const ctxMemoria = document.getElementById('rendimiento_ram');
        new Chart(ctxMemoria, {
            type: 'line',
            data: {
                labels: Array(valoresMemoria.length).fill(""),
                datasets: [
                    {
                        label: 'Memoria',
                        data: valoresMemoria,
                        backgroundColor: 'transparent',
                        borderColor: 'green',
                        borderWidth: 4
                    }
                ]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

        const ctxDisco = document.getElementById('rendimiento_disco');
        new Chart(ctxDisco, {
            type: 'line',
            data: {
                labels: Array(valoresDisco.length).fill(""),
                datasets: [
                    {
                        label: 'Disco',
                        data: valoresDisco,
                        backgroundColor: 'transparent',
                        borderColor: 'green',
                        borderWidth: 4
                    }
                ]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    }
}
