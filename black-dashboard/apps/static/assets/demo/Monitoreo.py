import json
import psutil
import time
from datetime import datetime

while True:
    # Obtener el consumo actual del equipo
    cpu = psutil.cpu_percent()
    memoria = psutil.virtual_memory().used / (1024 ** 3)  # Convertir a GB
    disco = psutil.disk_usage('C:\\').used / (1024 ** 3)  # Convertir a GB

    # Crear el diccionario con los datos actuales
    consumo_actual = {
        "fecha": datetime.now().isoformat(),
        "cpu": cpu,
        "memoria": f"{memoria:.2f} gb",
        "disco": f"{disco:.2f} gb"
    }

    # Leer los datos existentes
    try:
        with open('historico.json', 'r') as f:
            data = json.load(f)
    except FileNotFoundError:
        data = {"consumo_mensual": []}

    # Agregar los nuevos datos
    data["consumo_mensual"].append(consumo_actual)

    # Guardar los datos en el archivo JSON
    with open('historico.json', 'w') as f:
        json.dump(data, f, indent=4)

    # Imprimir los datos actuales
    print(json.dumps(consumo_actual, indent=4))

    # Esperar 10 segundos antes de la próxima iteración
    time.sleep(10)
